# Neutrino scattering with hot neutron matter 

The table lists the static denisty and spin structure factors in the long wavelength limit for hot neutron matter at different temperatures and densities, which can be used to compute the in-medium neutral-current neutrino-neutron scattering cross sections.






